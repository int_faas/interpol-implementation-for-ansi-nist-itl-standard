# INTERPOL's Implementation for ANSI/NIST ITL standard

This repository contains the draft version of INTERPOL's implementation for ANSI/NIST ITL.

The package includes:

*  the documentation for the standard
*  the xsd files describing the implementation
*  examples
*  the Machine Readable Tables describing the format
*  the ANSI/NIST umbrella standard INTERPOL's implementation is based on

## Getting the package
The package can be downloaded [here](https://bitbucket.org/int_faas/interpol-implementation-for-ansi-nist-itl-standard/get/06.00.00b.zip).

## Providing feedback or submitting issues
Feedback, questions, or issues can be submitted through the bugtracker [here](https://bitbucket.org/int_faas/interpol-implementation-for-ansi-nist-itl-standard/issues/new).

## Next milestones
This table lists the next milestones of the publication of this standard.

| Milestone                                                         | Tentative date        | Status |
|-------------------------------------------------------------------|-----------------------|--------|
| Publication of the draft version                                  | May 14th 2018         | Done   |
| End of feedback gathering                                         | September 15th 2018   | Done   |
| ~~Preparation of the official version, integrating the comments~~ | ~~October 15th 2018~~ | Decision to go for a second draft and a second feedback gathering period |
| ~~Publication of the official version, approved by the INTERPOL AFIS Expert Working Group~~ | ~~November 1st 2018~~ | |
| Publication of the 2nd draft version                              | January 7th 2019      |        |
| End of 2nd feeback gathering                                      | March, 31st, 2019     |        |
| Preparation of the official version, integrating the comments     | April 15th, 2019      |        |
| Publication of the official version, approved by the INTERPOL AFIS Expert Working Group   | May 1st, 2019 | |


